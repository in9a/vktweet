//
//  VTLoginViewController.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 25.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTLoginViewController.h"
#import "JTMaterialSwitch.h"
#import "UIColor+VTColors.h"
#import "VTTwitterClient.h"

@interface VTLoginViewController () <JTMaterialSwitchDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *switchContainerView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passworTextField;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;

@property (strong,nonatomic) JTMaterialSwitch *switcher;
@property (strong,nonatomic) UITextField * selectedTextField;
@property (strong,nonatomic) CAGradientLayer *gradientLayer;

@end

@implementation VTLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpController];
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    self.emailTextField.text = nil;
    self.passworTextField.text = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self startingAnimation];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //Gradient
    self.gradientLayer.frame = self.view.bounds;
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Action

- (void)startingAnimation{
    NSTimeInterval durationAnimation = 0.5;
    NSTimeInterval delayAnimation = 0.2;
    CGFloat spring = 0.6;
    CGFloat springInitial = 0.9;
    
    
    self.emailTextField.center = CGPointMake(self.emailTextField.center.x-self.view.frame.size.width,
                                             self.emailTextField.center.y);
    self.passworTextField.center= CGPointMake(self.passworTextField.center.x-self.view.frame.size.width,
                                              self.passworTextField.center.y);
    
    //Animate enterance of textFields
    [UIView animateWithDuration:durationAnimation delay:0 usingSpringWithDamping:spring initialSpringVelocity:springInitial options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        self.emailTextField.center= CGPointMake(self.emailTextField.center.x+self.view.frame.size.width,
                                                self.emailTextField.center.y);
        
    } completion:^(BOOL finished) {
        
        if (finished){
            [UIView animateWithDuration:durationAnimation delay:delayAnimation usingSpringWithDamping:spring initialSpringVelocity:springInitial options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                self.passworTextField.center= CGPointMake(self.passworTextField.center.x+self.view.frame.size.width,
                                                          self.passworTextField.center.y);
                
            } completion:^(BOOL finished) {
                
                
            }
             ];
        }
    }];
    
    //Animate enter button and switch
    [self addSwitch];
    
    self.switcher.alpha = 0;
    self.enterButton.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:durationAnimation delay:delayAnimation options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.switcher.alpha = 1.0;
        self.enterButton.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)tapAction:(UITapGestureRecognizer*)sender {
    
    [self.view endEditing:YES];
}

- (void)showAlertWithMessage:(NSString*)messgae{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:messgae preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
}

#pragma mark - setting Up

- (void)setUpController{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tap];
    
    //Gradient
    UIColor * color1 = [UIColor rgbColorWithRed:42 Green:163 Blue:240];
    UIColor * color2 = [UIColor rgbColorWithRed:88 Green:178 Blue:235];
    UIColor * color3 = [UIColor rgbColorWithRed:140 Green:192 Blue:231];
    UIColor * color4 = [UIColor rgbColorWithRed:225 Green:225 Blue:227];
    

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.colors = @[(id)color1.CGColor, (id)color2.CGColor, (id)color3.CGColor, (id)color4.CGColor];
    gradient.locations = @[@0.0, @0.25, @0.75, @1.0];
    gradient.frame = self.view.bounds;
    
    self.gradientLayer = gradient;
    self.view.backgroundColor = color2;
    
    [self.view.layer insertSublayer:self.gradientLayer atIndex:0];
}

- (void)addSwitch{
    JTMaterialSwitch * switcher = [[JTMaterialSwitch alloc] initWithSize:JTMaterialSwitchSizeBig style:JTMaterialSwitchStyleLight state:JTMaterialSwitchStateOff];
    switcher.center = self.switchContainerView.center;
    switcher.trackOnTintColor = [UIColor colorWithWhite:1 alpha:0.5];
    switcher.thumbOnTintColor = [UIColor rgbColorWithRed:240 Green:225 Blue:250];
    
    
    [self.switchContainerView addSubview:switcher];
    
    self.switcher = switcher;
    self.switcher.delegate = self;
}

- (void)setImageView:(UIImageView *)imageView{
    _imageView = imageView;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)switchStateChanged:(JTMaterialSwitchState)currentState{
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isEqual:self.emailTextField]) {
        [self.passworTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.passworTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.selectedTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.selectedTextField = nil;
}

#pragma mark - Keyboard handling

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
 
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.selectedTextField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.selectedTextField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - IBAction
- (IBAction)enterAction:(UIButton *)sender {
    
//    self.switcher.isOn
    
    if (self.switcher.isOn) {
        [self twitterLogin];
    } else {
        
    }
      
}

- (void)twitterLogin{
    [[[VTTwitterClient alloc]sharedClient] loginOnSuccess:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //dismiss vc
        });
        
        
    } onFailure:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAlertWithMessage:[error localizedDescription]];
        });
        
    }];
}
#pragma mark - Rotation

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         self.switcher.center = self.switchContainerView.center;
         
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context){
         
         
     }];
}

@end

