//
//  VTSplashViewController.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTSplashViewController.h"
#import "VTTwitterClient.h"
#import "VTUserTwitter.h"

typedef enum : NSUInteger {
    continueWithTwitter,
    continueWithVK,
    continueBoth,
} UserChoise;

@interface VTSplashViewController () <TwitterLoginDelegate>

@property (nonatomic)UserChoise type;

@end



@implementation VTSplashViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[VTTwitterClient alloc] sharedClient].delegate = self;
    
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self continueLoginTwitter];
    });
}


- (void)continueLoginTwitter{
    
    if ([VTUserTwitter getCurrentUser] != nil) {
        self.type = continueWithTwitter;
    }
    
//    else if (){
//        
//    }
    
    
}

-(void)goToApp{
    
}




@end


//goToLogin
//goToApp
