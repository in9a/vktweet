//
//  VTServerResponse.h
//  VkTweet
//
//  Created by Anton Kharchevskyi on 25.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VTServerResponse : NSObject
 
- (id)initWithServerResponse:(NSDictionary*)dictionary;

@end
