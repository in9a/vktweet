//
//  UIColor+VTColors.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 25.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "UIColor+VTColors.h"

@implementation UIColor (VTColors)

+ (UIColor*) rgbColorWithRed:(int)red Green:(int)green Blue:(int)blue{
    
    return [UIColor colorWithRed:(float)red/255.0 green:(float)green/255.0 blue:(float)blue/255.0 alpha:1];
    
}

@end
