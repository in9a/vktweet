//
//  VTUser.h
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTServerResponse.h"

typedef enum : NSUInteger {
    userTwitter,
    userVK,
} UserType;


extern NSString *const NotificationLogOutTwitter;

extern NSString *const NotificationLogOutVK;

@interface VTUser : VTServerResponse

@property (strong,nonatomic)NSDictionary *userDictionary;


- (void)saveUserToDefaults:(NSDictionary*)dictionary forUserType:(UserType)type;

+ (NSDictionary*)getDictionaryforType:(UserType)type;

+ (void)logOutForUserType:(UserType)type;

@end
