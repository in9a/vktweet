//
//  VTUser.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTUser.h"


static NSString *const twitterUserKey = @"twitterUser";
static NSString *const VkUserKey = @"VkUser";


NSString *const NotificationLogOutTwitter = @"NotificationLogOutTwiter";
NSString *const NotificationLogOutVK = @"NotificationLogOutVK";

@implementation VTUser


- (void)saveUserToDefaults:(NSDictionary*)dictionary forUserType:(UserType)type{
    NSError*error;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *key = twitterUserKey;
    
    if (type == userVK) {
        key = VkUserKey;
    }
    
    if (error == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:key];
    }
}

+ (NSDictionary*)getDictionaryforType:(UserType)type{
    
    
    NSString *key = twitterUserKey;
    
    if (type == userVK) {
        key = VkUserKey;
    }
    
    
    NSData * data = [[NSUserDefaults standardUserDefaults]objectForKey:key];
    NSError*error;
    
    if (data == nil) {
        return nil;
    }
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error == nil){
        
        return dict;
 
    }
    
    return nil;
}

+ (void)logOutForUserType:(UserType)type{
    
    NSString *key = twitterUserKey;
    
    if (type == userVK) {
        key = VkUserKey;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:key];
}



@end
