//
//  VTTwitterClient.h
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BDBOAuth1SessionManager.h"

@protocol TwitterLoginDelegate <NSObject>

- (void)continueLoginTwitter;

@end

@class BDBOAuth1SessionManager;
 

@interface VTTwitterClient : BDBOAuth1SessionManager

@property(weak, nonatomic) id <TwitterLoginDelegate> delegate;

- (VTTwitterClient*)sharedClient;

- (void)loginOnSuccess:(void(^)())successCompletion onFailure:(void(^)(NSError*))failureCompletion;

- (void)handleLogInFromURL:(NSURL*)url;

- (void)logoutTwitter;

@end
