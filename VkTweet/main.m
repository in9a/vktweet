//
//  main.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 25.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
