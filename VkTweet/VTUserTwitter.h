//
//  VTUserTwitter.h
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTUser.h"

@interface VTUserTwitter : VTUser

@property (strong,nonatomic)NSString*name;
@property (strong,nonatomic)NSString*screenName;
@property (strong,nonatomic)NSURL*profileUrl;
@property (strong,nonatomic)NSString*backgroundImage;
@property (nonatomic) NSInteger followers;
@property (nonatomic) NSInteger following;



+ (VTUserTwitter*)getCurrentUser;


@end
