//
//  UIColor+VTColors.h
//  VkTweet
//
//  Created by Anton Kharchevskyi on 25.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (VTColors)

+ (UIColor*) rgbColorWithRed:(int)red Green:(int)green Blue:(int)blue;

@end
