//
//  VTUserTwitter.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTUserTwitter.h"


@implementation VTUserTwitter

- (id)initWithServerResponse:(NSDictionary *)dictionary{
    self = [super initWithServerResponse:dictionary];
    
    if (self) {
    
        self.userDictionary = dictionary;
        
        self.name = dictionary[@"name"];
        self.screenName = dictionary[@"screen_name"];
        
        NSString *profile = dictionary[@"profile_image_url_https"];
        NSString *bigger = [profile stringByReplacingOccurrencesOfString:@"normal.png" withString:@"bigger.png"];
        
        self.profileUrl = [NSURL URLWithString:bigger];
        
        self.followers = [dictionary[@"followers_count"] integerValue];
        self.following = [dictionary[@"friends_count"] integerValue];
        
    }
    
    return self;
}

+ (VTUserTwitter*)getCurrentUser{
    
    NSDictionary *dict = [VTUser getDictionaryforType:userTwitter];
    
    VTUserTwitter *currentUser = [[VTUserTwitter alloc] initWithServerResponse:dict];
    
    return currentUser;
}


@end
