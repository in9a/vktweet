//
//  VTTwitterClient.m
//  VkTweet
//
//  Created by Anton Kharchevskyi on 26.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

#import "VTTwitterClient.h"

#import "VTUser.h"
#import "VTUserTwitter.h"


@interface VTTwitterClient ()

@property(strong,nonatomic) void (^loginSuccess)(void);

@property(strong,nonatomic) void (^loginFailure)(NSError*);

@end

@implementation VTTwitterClient
 
- (VTTwitterClient*)sharedClient{
 
    static VTTwitterClient *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[VTTwitterClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.twitter.com"]
                                                     consumerKey:@"WwV373UYvJGeIURcYIupemdYF"
                                                  consumerSecret:@"7bCAqoiiwkpVQZh8CEqRwhJZb7K71LzUwTfM4CbNMzGxSrXmDL"];

    });
    
    return sharedManager;
}

- (void)loginOnSuccess:(void(^)())successCompletion onFailure:(void(^)(NSError*error))failureCompletion{
    
    self.loginSuccess = successCompletion;
    self.loginFailure = failureCompletion;
    
    [self deauthorize];
    
    
    //getting link to redirect to safari(callback in info)
    [self fetchRequestTokenWithPath:@"oauth/request_token"
                             method:@"GET"
                        callbackURL:[NSURL URLWithString:@"tweettest://oauth"]
                              scope:nil
                            success:^(BDBOAuth1Credential *requestToken) {
                                
                                NSString *redirectUrl = [NSString stringWithFormat:@"https://api.twitter.com/oauth/authenticate?oauth_token=%@",requestToken.token];
                            
                                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:redirectUrl]];
                            }
                            failure:^(NSError *error) {
                                self.loginFailure(error);
                            }];
    
    //Get access token
}

- (void)handleLogInFromURL:(NSURL*)url{
    
    //make App Delegate boll to true
    
    BDBOAuth1Credential *requestToken = [BDBOAuth1Credential credentialWithQueryString:[url query]];
    
    
    [self fetchAccessTokenWithPath:@"oauth/access_token"
                            method:@"POST"
                      requestToken:requestToken
                           success:^(BDBOAuth1Credential *accessToken) {
                               //Get info for account
                               
                               [self GET:@"1.1/account/verify_credentials.json"
                              parameters:nil
                                progress:nil
                                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                     
                                     if ([responseObject isKindOfClass:[NSDictionary class]]){
                                         VTUserTwitter *user = [[VTUserTwitter alloc]initWithServerResponse:responseObject];
                                         [user saveUserToDefaults:user.userDictionary forUserType:userTwitter];
                                         
                                         self.loginSuccess();
                                         
                                         [self.delegate continueLoginTwitter];
                                     }
                                 }
                                 failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                     self.loginFailure(error);
                                 }];
                               
                               
                               
                           }
                           failure:^(NSError *error) {
                               self.loginFailure(error);
                           }];
}

- (void)logoutTwitter{
    [VTUser logOutForUserType:userTwitter];
    
    [self deauthorize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationLogOutTwitter object:nil];
}


@end
